import React from 'react';
import EmployeeData from './employee.json'
import CustomRow from '../CustomRow'

const home = () => {

  let emp = EmployeeData.employees.reduce((accumulator, element) => {
    if (!accumulator.find(el => el['id'] === element['id'])) {
      accumulator.push(element);
    }
    return accumulator;
  }, []);



  let rows = [];
  emp.forEach(e => {
    let { id, name, email } = e
    rows.push(<CustomRow id={id} name={name} email={email} key={rows.length} />);

  })

  return (
    <table>
      <thead>
        <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Email</th>
        </tr>
      </thead>
      <tbody>{rows}</tbody>
    </table>
  )

}


export default home;
