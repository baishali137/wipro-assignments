import React from 'react';

class CustomRow extends React.Component {
    render() {
        let { id, name, english, maths } = this.props
        return (

            <React.Fragment>
                <tr><td> {id}</td><td>{name}</td><td>{maths}</td><td>{english}</td></tr>
            </React.Fragment>



        );
    }
}
export default CustomRow

