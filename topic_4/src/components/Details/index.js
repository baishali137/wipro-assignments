import React from 'react'
import CustomRow from '../CustomRow';
import styled from 'styled-components'

const StyledTable = styled.table`
  border :1px dashed green;
  th{
    background:cyan;
  }
  th,td{
    padding:5px;
  }
  td{
    border:1px dashed green;
  }
`;
const Details = (props) => {
    const studentData = props.studentData;

    let rows = []
    studentData.forEach(item => {
        let { English, Maths } = item.studentMarks;

        rows.push(<CustomRow id={item.studentId} name={item.studentName} english={English} maths={Maths} key={rows.length} />)
    });

    return (
        <StyledTable><thead><tr><th> Student Id</th><th>Student Name</th><th> Maths</th><th>English</th></tr></thead><tbody>{rows}</tbody></StyledTable>
    )



}
export default Details