import React, { useState } from 'react'
import Header from '../Header'
import Details from '../Details'

const Main = () => {
    const [studentData, setStudentData] = useState(
        [
            {
                "studentId": "384489",
                "studentName": "Baishali Chakraborty",
                "studentMarks": {
                    "English": 60,
                    "Maths": 70
                }



            },
            {
                "studentId": "384490",
                "studentName": "Ujjaini Chakraborty",
                "studentMarks": {
                    "English": 60,
                    "Maths": 70
                }
            },
            {
                "studentId": "384491",
                "studentName": "Mala Chakraborty",
                "studentMarks": {
                    "English": 60,
                    "Maths": 70
                }
            }
        ]


    )

    return (
        <React.Fragment>
            <Header title="Student Details"></Header>
            <Details studentData={studentData}></Details>
        </React.Fragment>
    )
}
export default Main