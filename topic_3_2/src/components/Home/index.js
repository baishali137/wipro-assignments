import React from 'react';
import EmployeeData from './employee.json'
import CustomRow from '../CustomRow'
import styled from 'styled-components'

const StyledTable = styled.table`
  border :1px dashed green;
  th{
    background:cyan
  }
  th,td{
    padding:5px
  }
`;
const home = () => {
  let emp = EmployeeData.employees.reduce((accumulator, element) => {
    if (!accumulator.find(el => el['id'] === element['id'])) {
      accumulator.push(element);
    }
    return accumulator;
  }, []);



  let rows = [];
  emp.forEach(e => {
    let { id, name, email } = e
    rows.push(<CustomRow id={id} name={name} email={email} key={rows.length} />);

  })

  return (
    <React.Fragment>
      <StyledTable>
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Email</th>
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </StyledTable>
    </React.Fragment>

  )

}


export default home;
