import React from 'react';

class CustomRow extends React.Component {
    render() {
        return (
            <React.Fragment>
                <tr>
                    <td> {this.props.id}</td>
                    <td>{this.props.name}</td>
                    <td>{this.props.email}</td>
                </tr>
            </React.Fragment>



        );
    }
}
export default CustomRow

