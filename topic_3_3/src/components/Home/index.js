import React from 'react';
import MultiplyData from './multiplier.json'
import CustomRow from '../CustomRow'
import styled from 'styled-components'

const StyledHeader = styled.h1`
font-size:20px;
color:#333;
`;
const StyledTable = styled.table`
  border :1px dashed green;
  th{
    background:cyan;
  }
  th,td{
    padding:5px;
  }
  td{
    border:1px dashed green;
  }
`;
const home = () => {

  let mul = MultiplyData.multipliers


  let rows = [];
  mul.map((m) => {
    let num = 5
    rows.push(<CustomRow num={num} mul={m} val={num * m} key={rows.length} />);
    return rows
  })

  return (
    <React.Fragment>
      <StyledHeader> Multiplication Table:5</StyledHeader>
      <StyledTable>

        <tbody>{rows}</tbody>
      </StyledTable>
    </React.Fragment>

  )

}


export default home;
